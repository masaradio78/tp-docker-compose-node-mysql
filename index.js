require('dotenv').config();
const express = require('express');
const routes = require('./routes');
const getConnection = require('./database');
const startMigration = require('./migrations');

async function bootstrap() {
    const connection = await getConnection();
    const check = await connection.query(`SELECT COUNT(*) as count FROM information_schema.tables WHERE table_schema = "${process.env.DB_NAME}"`)
    const countTables = JSON.parse(JSON.stringify(check[0]))[0].count;
    if (countTables === 0) {
        const result = await startMigration(connection);
        if (!result) {
            return;
        }
    }
    const app = express();

    routes(app, connection);
    await app.listen(process.env.PORT, () => console.log(`Listening on ${process.env.PORT}`))
}

bootstrap();
