const User = require('../models').User;

class UserController{

    static async createUser(connection, data) {
        const {firstName, lastName, email, age} = data;
        if (firstName == null || lastName == null || email == null || age == null) {
            return null;
        }
        const newUser = new User(firstName, lastName, email, age);
        try {
            await connection.execute(
                "INSERT INTO user (firstName, lastName, email, age) VALUES (?, ?, ?, ?)",
                [firstName, lastName, email, age]
            );
            return newUser;
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    static async getUsers(connection) {
        try {
            const query = await connection.execute("SELECT * FROM user");
            return JSON.parse(JSON.stringify(query))[0];
        } catch (e) {
            console.error(e);
            return null;
        }
    }
}

module.exports = UserController;
