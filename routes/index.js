const userRoutes = require('./user.route');

module.exports = function(app, connection) {
    userRoutes(app, connection);
};
