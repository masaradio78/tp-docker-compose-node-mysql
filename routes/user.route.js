const bodyParser = require('body-parser');
const userController = require('../controllers').UserController;

module.exports = function (app, connection) {
    app.post('/users', bodyParser.json(),
        async (req, res) => {
            const data = req.body;
            try {
                const user = await userController.createUser(connection, data)
                if (user == null) {
                    res.status(400).end();
                    return;
                }
                res.status(201).json(user);
            } catch (e) {
                console.error(e);
                res.status(500).end();
            }
        });
    app.get('/users', async(req, res) => {
       try {
           const users = await userController.getUsers(connection);
           res.status(200).json(users);
       } catch (e) {
           console.error(e);
           res.status(500).end();
       }
    });
};
