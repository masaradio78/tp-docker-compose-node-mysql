require('dotenv').config();
const mysql = require('mysql2');

module.exports = async function getConnection() {
    return await mysql.createConnectionPromise({
        host: 'localhost',
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    })
};

