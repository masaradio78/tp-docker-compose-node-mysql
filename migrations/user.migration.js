module.exports = async function(connection) {
    try {
        await connection.query('CREATE TABLE user (' +
            'id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,' +
            'firstName VARCHAR(255),' +
            'lastName VARCHAR(255),' +
            'email VARCHAR(255),' +
            'age INT)');
    } catch (e) {
        console.error(e);
        return false;
    }
    return true;
};
