const userMigration = require('./user.migration');

module.exports = async function(connection) {
  return await userMigration(connection);
};
